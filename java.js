//1-- Pegar dados do raio



function calcularRaio() {
    let raio = document.getElementById("idCircRaio").value;
    let area = Math.PI * Math.pow(raio, 2);


    document.getElementById("idOutCirc").value = "O cálculo da área é: " + area.toFixed(2);



}

//2-- Pegar dados da AxB (Retangulo)


function calculoRet() {
    let altura = document.getElementById("idRetAlt").value;
    let base = document.getElementById("idRetBas").value;

    let resultadoRet = (altura * base);

    document.getElementById("idOutRet").value = "O cálculo de área é: " + resultadoRet;



}
//3-- Pegar dados AxB(Triangulo Retagulo)



function calculoTriRet() {
    let altura = document.getElementById("idTrReAlt").value;
    let base = document.getElementById("idTrReBas").value;

    let resultadoTriRet = (altura * base) / 2;

    document.getElementById("idOutTriRet").value = "O cálculo da área é: " + resultadoTriRet;

}



//4-- Pegar dados LxLxL(Triangulo Equilatero)

function calculoTriEqu() {
    let lado1 = document.getElementById("idTrEq1").value;
    let lado2 = document.getElementById("idTrEq2").value;
    let lado3 = document.getElementById("idTrEq3").value;

    let resultadoTriEqu = (lado1 + lado2 + lado3) / 4;

    document.getElementById("idOutTriEq").value = "O cálculo da área é: " + resultadoTriEqu;

}







